package com.company;

import java.util.ArrayList;
import java.util.List;

public class Baralho {

    List<Carta> cartas = new ArrayList<>();

    public Baralho() {
        for(Naipe naipe: Naipe.values()){
            for(LetraCarta letraCarta: LetraCarta.values()){
                cartas.add(new Carta(letraCarta, naipe));
            }
        }
    }
}
