package com.company;

public class Carta {
    LetraCarta numero;
    Naipe carta;

    public LetraCarta getNumero() {
        return numero;
    }

    public void setNumero(LetraCarta numero) {
        this.numero = numero;
    }

    public Naipe getCarta() {
        return carta;
    }

    public void setCarta(Naipe carta) {
        this.carta = carta;
    }

    public Carta(LetraCarta numero, Naipe carta) {
        this.numero = numero;
        this.carta = carta;
    }
}
