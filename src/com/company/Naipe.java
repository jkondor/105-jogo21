package com.company;

public enum Naipe {
    OUROS("ouros"),
    COPAS("copas"),
    PAUS("paus"),
    ESPADAS("espadas"),
    ;

    private String nomeNaipe;

    Naipe(String nomeNaipe) {
        this.nomeNaipe = nomeNaipe;
    }

    public String getNomeNaipe() {
        return nomeNaipe;
    }
}
